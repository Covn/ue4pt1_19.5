﻿#include <iostream>
#include <string>
using namespace std;
class Animal
{
public:
    virtual void voice(){}
};

class Dog : public Animal
{
public:
    void voice() override
    {
        cout << "woof"<< "\n";
    }
};
class Cat : public Animal
{
public:
    void voice() override
    {
        cout << "mew" << "\n";
    }
};
class Cow : public Animal
{
public:
    void voice() override
    {
        cout << "moo" << "\n";
    }
};

int main()
{
    int N=3;
    Animal* ZOO[3] = {new Dog,new Cat, new Cow};
    for (int i = 0; i < 3; i++)
    {
        ZOO[i]->voice();
    }
}